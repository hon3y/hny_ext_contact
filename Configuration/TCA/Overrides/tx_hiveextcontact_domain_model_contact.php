<?php

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$sModel = 'tx_hiveextcontact_domain_model_contact';

$GLOBALS['TCA'][$sModel]['columns']['gender']['config']['items'] = array(
	array('-- Bitte Auswählen --', ),
	array('männlich', 1),
	array('weiblich', 2)
);

$GLOBALS['TCA'][$sModel]['columns']['hive_ext_country']['config']['foreign_table_where'] =
    "AND tx_hiveextcountry_domain_model_country.hidden=0 AND tx_hiveextcountry_domain_model_country.deleted=0 AND tx_hiveextcountry_domain_model_country.sys_language_uid IN (-1,0)";

$GLOBALS['TCA'][$sModel]['columns']['sys_language_mm'] = [
    'exclude' => 0,
    'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/translation_db.xlf:sysfilemetadata.show_in_language',
    'config' => [
        'type' => 'select',
        'renderType' => 'selectMultipleSideBySide',
        'foreign_table' => 'sys_language',
        'MM' => 'tx_hiveextcontact_domain_model_contact_sys_language_mm',
        'size' => 10,
        'autoSizeMax' => 30,
        'maxitems' => 9999,
        'multiple' => 0,
        'enableMultiSelectFilterTextfield' => TRUE,
        'wizards' => [
            '_PADDING' => 1,
            '_VERTICAL' => 1,
        ],
    ],
];



$GLOBALS['TCA'][$sModel]['types']['1']['showitem'] = '--div--;General,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource,
--div--;Person,gender, title, firstname, middlename, lastname, birthdate, position, description,
--div--;Image,image, focus_x, focus_y,
--div--;Contact,phone, fax, mobile, email, supporttext, supportlink, building, room, department, company, social_media,
--div--;Countries and Languages,hive_ext_country, belongs_to_default_sys_language, sys_language_mm,
--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden;;1, starttime, endtime';

$ConfigsPaletteImage = array(
	'0' => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
	\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
		'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,--palette--;;filePalette'
	),
);
$GLOBALS['TCA']['tx_hiveextcontact_domain_model_contact']['columns']['image']['config']['foreign_types'] = $ConfigsPaletteImage;


$GLOBALS['TCA']['tx_hiveextcontact_domain_model_contact']['ctrl']['label'] = 'lastname';
$GLOBALS['TCA']['tx_hiveextcontact_domain_model_contact']['ctrl']['label_alt'] = 'firstname, position';
$GLOBALS['TCA']['tx_hiveextcontact_domain_model_contact']['ctrl']['label_alt_force'] =  1;


$GLOBALS['TCA']['tx_hiveextcontact_domain_model_contact']['columns']['supportlink']['config'] = [
	'type' => 'input',
	'size' => 20,
	'eval' => 'trim',
	'wizards' => [
		'_PADDING' => 2,
		'link' => [
			'type' => 'popup',
			'title' => 'Link',
			'icon' => 'link_popup.gif',
			'module' => [
				'name' => 'wizard_element_browser',
				'urlParameters' => [
					'mode' => 'wizard'
				]
			],
			'JSopenParams' => 'height=800,width=600,status=0,menubar=0,scrollbars=1'
		],
	],
];

$GLOBALS['TCA'][$sModel]['columns']['email']['config'] = [
	'type' => 'input',
	'size' => 20,
	'eval' => 'trim',
	'wizards' => [
		'_PADDING' => 2,
		'link' => [
			'type' => 'popup',
			'title' => 'Link',
			'icon' => 'actions-wizard-link',
			'module' => [
				'name' => 'wizard_link',
				'urlParameters' => [
					'mode' => 'wizard'
				]
			],
			'JSopenParams' => 'height=800,width=600,status=0,menubar=0,scrollbars=1',
			'params' => [
				'blindLinkOptions' => 'page, folder, file, url',
				'blindLinkFields' => 'class, params',

			],
		],
	],
	'softref' => 'typolink'
];
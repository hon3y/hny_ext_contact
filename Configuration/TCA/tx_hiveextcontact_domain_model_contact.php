<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact',
        'label' => 'gender',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'gender,title,firstname,middlename,lastname,image,focus_x,focus_y,birthdate,position,description,phone,fax,mobile,email,supporttext,supportlink,building,room,belongs_to_default_sys_language,department,company,social_media,hive_ext_country',
        'iconfile' => 'EXT:hive_ext_contact/Resources/Public/Icons/tx_hiveextcontact_domain_model_contact.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, gender, title, firstname, middlename, lastname, image, focus_x, focus_y, birthdate, position, description, phone, fax, mobile, email, supporttext, supportlink, building, room, belongs_to_default_sys_language, department, company, social_media, hive_ext_country',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, gender, title, firstname, middlename, lastname, image, focus_x, focus_y, birthdate, position, description, phone, fax, mobile, email, supporttext, supportlink, building, room, belongs_to_default_sys_language, department, company, social_media, hive_ext_country, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hiveextcontact_domain_model_contact',
                'foreign_table_where' => 'AND tx_hiveextcontact_domain_model_contact.pid=###CURRENT_PID### AND tx_hiveextcontact_domain_model_contact.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'gender' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.gender',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'firstname' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.firstname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'middlename' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.middlename',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lastname' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.lastname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'image' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'focus_x' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.focus_x',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'focus_y' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.focus_y',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'birthdate' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.birthdate',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'datetime',
                'default' => time()
            ],
        ],
        'position' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.position',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'phone' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.phone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'fax' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.fax',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'mobile' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.mobile',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'email' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'supporttext' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.supporttext',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'supportlink' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.supportlink',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'building' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.building',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'room' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.room',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'belongs_to_default_sys_language' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.belongs_to_default_sys_language',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'department' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.department',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_hiveextcontact_domain_model_department',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'company' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.company',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_hiveextcontact_domain_model_company',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'social_media' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.social_media',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_hiveextcontact_domain_model_socialmedia',
                'minitems' => 0,
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'hive_ext_country' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_contact.hive_ext_country',
            'config' => [
                'type' => 'select',
                'renderType' => '',
                'foreign_table' => 'tx_hiveextcountry_domain_model_country',
                'MM' => 'tx_hiveextcontact_contact_country_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'module' => [
                            'name' => 'wizard_edit',
                        ],
                        'type' => 'popup',
                        'title' => 'Edit', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.edit
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'module' => [
                            'name' => 'wizard_add',
                        ],
                        'type' => 'script',
                        'title' => 'Create new', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.add
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                        'params' => [
                            'table' => 'tx_hiveextcountry_domain_model_country',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                    ],
                ],
            ],

        ],

    ],
];

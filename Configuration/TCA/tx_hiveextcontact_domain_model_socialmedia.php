<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_socialmedia',
        'label' => 'facebook',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'facebook,you_tube,twitter,linked_in,pinterest,instagram,google_plus,xing',
        'iconfile' => 'EXT:hive_ext_contact/Resources/Public/Icons/tx_hiveextcontact_domain_model_socialmedia.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, facebook, you_tube, twitter, linked_in, pinterest, instagram, google_plus, xing',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, facebook, you_tube, twitter, linked_in, pinterest, instagram, google_plus, xing, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hiveextcontact_domain_model_socialmedia',
                'foreign_table_where' => 'AND tx_hiveextcontact_domain_model_socialmedia.pid=###CURRENT_PID### AND tx_hiveextcontact_domain_model_socialmedia.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'facebook' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_socialmedia.facebook',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'you_tube' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_socialmedia.you_tube',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'twitter' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_socialmedia.twitter',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'linked_in' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_socialmedia.linked_in',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'pinterest' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_socialmedia.pinterest',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'instagram' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_socialmedia.instagram',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'google_plus' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_socialmedia.google_plus',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'xing' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_contact/Resources/Private/Language/locallang_db.xlf:tx_hiveextcontact_domain_model_socialmedia.xing',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
    
    ],
];


plugin.tx_hiveextcontact_contactlist {
    view {
        # cat=plugin.tx_hiveextcontact_contactlist/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_contact/Resources/Private/Templates/
        # cat=plugin.tx_hiveextcontact_contactlist/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_contact/Resources/Private/Partials/
        # cat=plugin.tx_hiveextcontact_contactlist/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_contact/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextcontact_contactlist//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries {
    view {
        # cat=plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_contact/Resources/Private/Templates/
        # cat=plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_contact/Resources/Private/Partials/
        # cat=plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_contact/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextcontact_contactlistforcurrentsyslanguagebycountries//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

//plugin.tx_hiveextcontact_contactlist {
//    settings {
//        production {
//            includePath {
//            public = EXT:hive_ext_contact/Resources/Public/
//                private = EXT:hive_ext_contact/Resources/Private/
//                frontend {
//                public = typo3conf/ext/hive_ext_contact/Resources/Public/
//                }
//            }
//        }
//    }
//}
//plugin.tx_hiveextcontact_contactlist.persistence.storagePid = 53

plugin {
    tx_hiveextcontact {

        persistence {
            storagePid =
        }

        model {
            HIVE\HiveExtContact\Domain\Model\Contact {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtContact\Domain\Model\Abstract {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtContact\Domain\Model\Company {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtContact\Domain\Model\Department {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtContact\Domain\Model\SocialMedia {
                persistence {
                    storagePid =
                }
            }
        }
    }
}

<?php
namespace HIVE\HiveExtContact\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_contact" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * SocialMedia
 */
class SocialMedia extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * facebook
     *
     * @var string
     */
    protected $facebook = '';

    /**
     * youTube
     *
     * @var string
     */
    protected $youTube = '';

    /**
     * twitter
     *
     * @var string
     */
    protected $twitter = '';

    /**
     * linkedIn
     *
     * @var string
     */
    protected $linkedIn = '';

    /**
     * pinterest
     *
     * @var string
     */
    protected $pinterest = '';

    /**
     * instagram
     *
     * @var string
     */
    protected $instagram = '';

    /**
     * googlePlus
     *
     * @var string
     */
    protected $googlePlus = '';

    /**
     * xing
     *
     * @var string
     */
    protected $xing = '';

    /**
     * Returns the facebook
     *
     * @return string $facebook
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Sets the facebook
     *
     * @param string $facebook
     * @return void
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * Returns the youTube
     *
     * @return string $youTube
     */
    public function getYouTube()
    {
        return $this->youTube;
    }

    /**
     * Sets the youTube
     *
     * @param string $youTube
     * @return void
     */
    public function setYouTube($youTube)
    {
        $this->youTube = $youTube;
    }

    /**
     * Returns the twitter
     *
     * @return string $twitter
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Sets the twitter
     *
     * @param string $twitter
     * @return void
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * Returns the linkedIn
     *
     * @return string $linkedIn
     */
    public function getLinkedIn()
    {
        return $this->linkedIn;
    }

    /**
     * Sets the linkedIn
     *
     * @param string $linkedIn
     * @return void
     */
    public function setLinkedIn($linkedIn)
    {
        $this->linkedIn = $linkedIn;
    }

    /**
     * Returns the pinterest
     *
     * @return string $pinterest
     */
    public function getPinterest()
    {
        return $this->pinterest;
    }

    /**
     * Sets the pinterest
     *
     * @param string $pinterest
     * @return void
     */
    public function setPinterest($pinterest)
    {
        $this->pinterest = $pinterest;
    }

    /**
     * Returns the instagram
     *
     * @return string $instagram
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * Sets the instagram
     *
     * @param string $instagram
     * @return void
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;
    }

    /**
     * Returns the googlePlus
     *
     * @return string $googlePlus
     */
    public function getGooglePlus()
    {
        return $this->googlePlus;
    }

    /**
     * Sets the googlePlus
     *
     * @param string $googlePlus
     * @return void
     */
    public function setGooglePlus($googlePlus)
    {
        $this->googlePlus = $googlePlus;
    }

    /**
     * Returns the xing
     *
     * @return string $xing
     */
    public function getXing()
    {
        return $this->xing;
    }

    /**
     * Sets the xing
     *
     * @param string $xing
     * @return void
     */
    public function setXing($xing)
    {
        $this->xing = $xing;
    }
}

<?php
namespace HIVE\HiveExtContact\Controller;

/***
 *
 * This file is part of the "hive_ext_contact" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * ContactController
 */
class ContactController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * contactRepository
     *
     * @var \HIVE\HiveExtContact\Domain\Repository\ContactRepository
     * @inject
     */
    protected $contactRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        //get settings
        $aSettings = $this->settings;
        if (array_key_exists('bDebug', $aSettings) && array_key_exists('sDebugIp', $aSettings)) {
            $aDebugIp = explode(',', $aSettings['sDebugIp']);
            if ($aSettings['bDebug'] == '1' && (in_array($_SERVER['REMOTE_ADDR'], $aDebugIp) or $aSettings['sDebugIp'] == '*')) {
                \TYPO3\CMS\Core\Utility\DebugUtility::debug($aSettings, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
            }
        }

        //get plugin uid
        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];

        //get contacts if not empty
        $contacts = $this->contactRepository->findByUidListOrderByListIfNotEmpty($aSettings['oHiveExtContact']['main']['mn']);

        $this->view->assign('contacts', $contacts);
        $this->view->assign('aSettings', $aSettings);
        $this->view->assign('iPluginUid', $iPluginUid);
    }

//    /**
//     * action list
//     *
//     * @return void
//     */
//    public function listAction()
//    {
//        $contacts = $this->contactRepository->findAll();
//        $this->view->assign('contacts', $contacts);
//    }

    /**
     * action show
     *
     * @param \HIVE\HiveExtContact\Domain\Model\Contact $contact
     * @return void
     */
    public function showAction(\HIVE\HiveExtContact\Domain\Model\Contact $contact)
    {
        $this->view->assign('contact', $contact);
    }

    /**
     * action listForCurrentSysLanguageByCountries
     *
     * @return void
     */
    public function listForCurrentSysLanguageByCountriesAction()
    {

    }
}
